# Orangesite Transparency Log

Live: https://orangesite.sneak.cloud

Shows stories that were on the orangesite front page within the last 24
hours, but aren't any more.  Sorted by when they exited the frontpage, most
recent first.

Stories on the frontpage for less than a half hour (likely manually
moderator nuked if rank &gt; 25 or so) are marked red for convenience.

# TODO

* continue to resist the urge to use the orange

# Author

* [sneak@sneak.berlin](mailto:sneak@sneak.berlin)
