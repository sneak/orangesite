module git.eeqj.de/sneak/orangesite

go 1.14

require (
	git.eeqj.de/sneak/goutil v0.0.0-20200330224956-7fad5dc142e5
	github.com/flosch/pongo2 v0.0.0-20190707114632-bbf5a6c351f4
	github.com/hako/durafmt v0.0.0-20191009132224-3f39dc1ed9f4
	github.com/jinzhu/gorm v1.9.12
	github.com/juju/errors v0.0.0-20200330140219-3fe23663418f // indirect
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/echo/v4 v4.1.16 // indirect
	github.com/labstack/gommon v0.3.0
	github.com/mattn/go-isatty v0.0.12
	github.com/mattn/go-sqlite3 v2.0.3+incompatible // indirect
	github.com/mayowa/echo-pongo2 v0.0.0-20170410154925-661ce95e1767
	github.com/peterhellberg/hn v0.0.0-20160106115829-a27cdd2ca854
	github.com/rs/zerolog v1.18.0
	github.com/ziflex/lecho/v2 v2.0.0
	golang.org/x/crypto v0.0.0-20200323165209-0ec3e9974c59 // indirect
	golang.org/x/net v0.0.0-20200324143707-d3edc9973b7e // indirect
	golang.org/x/sys v0.0.0-20200327173247-9dae0f8f5775 // indirect
)
