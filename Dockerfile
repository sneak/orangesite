FROM golang:1.14 as builder

WORKDIR /go/src/git.eeqj.de/sneak/orangesite
COPY . .

#RUN make lint && make build
RUN make build

WORKDIR /go
RUN tar cvfz go-src.tgz src && du -sh *

# this container doesn't do anything except hold the build artifact
# and make sure it compiles.

FROM alpine

RUN mkdir -p /app/bin

COPY --from=builder /go/src/git.eeqj.de/sneak/orangesite/server /app/bin/server
# FIXME figure out how to embed these stupid templates
COPY --from=builder /go/src/git.eeqj.de/sneak/orangesite/view /app/view

# put the source in there too for safekeeping
COPY --from=builder /go/go-src.tgz /usr/local/src/go-src.tgz

# this is where the db gets stored:
VOLUME /data

WORKDIR /app
CMD /app/bin/server

# FIXME add testing
