//go:generate go run github.com/UnnoTed/fileb0x b0x.yaml
package main

import (
	"os"

	"git.eeqj.de/sneak/orangesite/hn"
)

var Version string
var Buildarch string

func main() {
	os.Exit(hn.RunServer(Version, Buildarch))
}
