package hn

import (
	"net/http"
	"time"

	u "git.eeqj.de/sneak/goutil"
	"github.com/flosch/pongo2"
	"github.com/jinzhu/gorm"
	"github.com/labstack/echo"
)

type RequestHandlerSet struct {
	db      *gorm.DB
	version string
}

func NewRequestHandlerSet(version string, db *gorm.DB) *RequestHandlerSet {
	rhs := new(RequestHandlerSet)
	rhs.db = db
	rhs.version = version
	return rhs
}

func (r *RequestHandlerSet) indexHandler(c echo.Context) error {
	last24h := time.Now().Add(time.Second * 86400 * -1)
	var fpi []HNFrontPage
	r.db.Where("disappeared is not ? and disappeared > ?", time.Time{}, last24h).Order("disappeared desc").Find(&fpi)

	type fprow struct {
		Duration     string
		DurationSecs uint
		URL          string
		Title        string
		HighestRank  uint
		HNID         uint
		Score        uint
		TimeGone     string
		TimeGoneSecs uint
	}
	var fprows []fprow

	for _, item := range fpi {
		fprows = append(fprows, fprow{
			Duration:     u.TimeDiffHuman(item.Disappeared, item.Appeared),
			DurationSecs: u.TimeDiffAbsSeconds(item.Disappeared, item.Appeared),
			URL:          item.URL,
			HNID:         item.HNID,
			Score:        item.Score,
			Title:        item.Title,
			HighestRank:  item.HighestRank,
			TimeGone:     u.TimeDiffHuman(time.Now(), item.Disappeared),
			TimeGoneSecs: u.TimeDiffAbsSeconds(time.Now(), item.Disappeared),
		})
	}

	type rowtwo struct {
		Duration     string
		DurationSecs uint
		URL          string
		Title        string
		Score        uint
		HighestRank  uint
		HNID         uint
		Rank         uint
	}
	var currentfp []rowtwo

	var cur []HNFrontPage
	r.db.Where("disappeared is ?", time.Time{}).Order("rank asc").Find(&cur)

	for _, item := range cur {
		currentfp = append(currentfp, rowtwo{
			Duration:     u.TimeDiffHuman(time.Now(), item.Appeared),
			DurationSecs: u.TimeDiffAbsSeconds(time.Now(), item.Appeared),
			URL:          item.URL,
			HNID:         item.HNID,
			Score:        item.Score,
			Title:        item.Title,
			HighestRank:  item.HighestRank,
			Rank:         item.Rank,
		})
	}

	tc := pongo2.Context{
		"time":    time.Now().UTC().Format(time.RFC3339Nano),
		"exits":   fprows,
		"current": currentfp,
		"gitrev":  r.version,
	}
	return c.Render(http.StatusOK, "index.html", tc)
}

func (r *RequestHandlerSet) aboutHandler(c echo.Context) error {
	tc := pongo2.Context{
		"time":   time.Now().UTC().Format(time.RFC3339Nano),
		"gitrev": r.version,
	}
	return c.Render(http.StatusOK, "about.html", tc)
}
