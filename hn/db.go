package hn

import (
	"time"

	"github.com/jinzhu/gorm"
)

// this schema is quite redundant, i know

type HNFrontPage struct {
	gorm.Model
	InternalID  uint64 `gorm:"primary_key;auto_increment:true`
	HNID        uint   // HN integer id
	Appeared    time.Time
	Disappeared time.Time
	HighestRank uint   // frontpage index
	Rank        uint   // frontpage index
	Title       string // submission title
	Score       uint   // updoots
	URL         string // duh
}

type HNStoryRank struct {
	gorm.Model
	InternalStoryID uint64    `gorm:"primary_key;auto_increment:true`
	HNID            uint      // HN integer id
	Title           string    // submission title
	URL             string    // duh
	Rank            uint      // frontpage index
	Score           uint      // updoots
	FetchedAt       time.Time // identical within fetchid
}

type FrontPageCache struct {
	gorm.Model
	CacheID            uint64 `gorm:"primary_key;auto_increment:true`
	HNID               uint
	HighestRankReached uint
	URL                string
	Title              string
	Appeared           time.Time
	Disappeared        time.Time
}
